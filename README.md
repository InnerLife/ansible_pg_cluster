Ansible playbook for a fault tolerant PostgreSQL cluster using repmgr and barman.

[MORE ABOUT CLUSTER](http://innerlife.io/fault-tolerant-postgresql-cluster-part1)

Tesed on Debian 9, Ansible 2.2.1.0.

### Installation
1) Prepare environment
```sh
apt install ansible
ssh-keygen -b 2048 -t rsa -N "" -C "root@ansible"
ssh-copy-id root@db1_host
ssh-copy-id root@db2_host
ssh-copy-id root@backup_host
```
2) Change IPs inside hosts file.

3) Run:
```sh
ansible-playbook -i hosts --extra-vars="@vars.yml" main.yml
```

### Advanced
Install PostgreSQL on db hosts:
```sh
ansible-playbook -i hosts --extra-vars="@vars.yml" main.yml --tags "pg"
```
All above + join db hosts using OpenVPN:
```sh
ansible-playbook -i hosts --extra-vars="@vars.yml" main.yml --tags "pg,link_dbs"
```
All above + configure asynchronous replication using repmgr:
```sh
ansible-playbook -i hosts --extra-vars="@vars.yml" main.yml --tags "pg,link_dbs,repmgr"
```
All above + join all servers using OpenVPN:
```sh
ansible-playbook -i hosts --extra-vars="@vars.yml" main.yml --tags "pg,link_dbs,repmgr,link_all"
```
All above + configure auto-failover using repmgr daemons and install test PostgreSQL instance on backup host:
```sh
ansible-playbook -i hosts --extra-vars="@vars.yml" main.yml --tags "pg,link_dbs,repmgr,link_all,repmgrd"
```
All above + configure HA using repmgr daemons events:
```sh
ansible-playbook -i hosts --extra-vars="@vars.yml" main.yml --tags "pg,link_dbs,repmgr,link_all,repmgrd,repmgrd_ha"
```
All above + configure barman and change replication between db servers to synchronous:
```sh
ansible-playbook -i hosts --extra-vars="@vars.yml" main.yml
```
### Vars
```sh
host_vars/db1
host_vars/db2
host_vars/backup
vars.yml
```